# Repositorio ejemplos de la formación en pruebas automatizadas con Protractor

En este repositorio se encuentran los siguientes ejemplos

## Ejemplo 1: test-ciclo-protractor

A través de éste ejemplo veremos como se comporta protractor a la hora de la ejecución de las pruebas, mostrando el ciclo que se desarrolla y como controlar el orden de ejecución de las pruebas.

## Ejemplo 2: Demo de login de la aplicación

Nos loguearemos en la aplicación y mostraremos las consideraciones previas que debemos de tener debido a los tiempos de respuesta del API.

## Ejemplo 3: Test con parámetros

Volveremos a repetir el test anterior pero pasándole varios valores por parámetros.