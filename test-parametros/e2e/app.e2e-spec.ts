import { AppPage } from './app.po';
import { browser, ExpectedConditions } from 'protractor';

const page: AppPage = new AppPage();
const EC = ExpectedConditions;

describe('Test2 App, prueba de pase por parámetros', () => {
  it('debemos acceder a la página del login', () => {
    const url = 'http://h15973.redocu.lan/develop/'
    page.navigateToLogin(url);
    browser.wait(EC.visibilityOf(page.inputUsuario), 25000);
  });
  // TODO: Mostrar comportamiento de parámetros
  it('debemos loguearnos y acceder a la pantalla de Inicio', () => {
    console.log('User: ', browser.params.login.user);
    console.log('Pass: ', browser.params.login.pass);
    expect(page.loginApp(browser.params.login.user, browser.params.login.pass)).toContain('/inicio');
  });
});
