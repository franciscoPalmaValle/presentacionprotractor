import { AppPage } from './app.po';
import { browser, ExpectedConditions, element, by, $$ } from 'protractor';
import { ListadoFormacionAcademica } from './listadoFA.po';

const page: AppPage = new AppPage();
const listadoFA: ListadoFormacionAcademica = new ListadoFormacionAcademica();
const EC = ExpectedConditions;
const url = 'http://h15973.redocu.lan/develop/'
const timeOut = 25000;
const tipoAct = 'Doctorado'

describe('Test1 App. Prueba base app', () => {
  it('debemos acceder a la página del login', () => {
    page.navigateToLogin(url);
    browser.wait(EC.visibilityOf(element(by.id('usuario'))), timeOut);
  });
  it('debemos loguearnos y acceder a la pantalla de Inicio', () => {
    expect(page.loginApp('rxalabarder', 'a')).toContain('/inicio');
    browser.wait(EC.visibilityOf(element(by.id('usuario-info'))), timeOut);
  });
  it('comprobamos que podemos acceder al listado de Formación académica', () => {
    browser.get(url + 'uxxi-investigacion/curriculum/datos-investigador/formacion-academica');
    browser.wait(EC.visibilityOf(element(by.cssContainingText('.breadcrumb-title', 'Formación académica'))), timeOut);
  });
  it('comprobamos que al pulsar en Crear se despliega el modal de elección de tipo de Formación académica', () => {
    listadoFA.botonCrear.click();
    browser.wait(EC.visibilityOf(listadoFA.opcionesFormacionAcademica.first()), timeOut);
  });
  it('clicamos en el botón de Doctorados', () => {
    page.filtrarModal(tipoAct).click();
    browser.sleep(5000);
    expect($$('.breadcrumb-items').last().getText()).toContain(tipoAct);
  });
});