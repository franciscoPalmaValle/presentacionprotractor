import { $, by, ElementArrayFinder, ElementFinder } from 'protractor';

export class ListadoFormacionAcademica {

    botonCrear: ElementFinder;
    opcionTesinaListado: ElementFinder;
    opcionesFormacionAcademica: ElementArrayFinder;
    botonCerrarModal: ElementFinder;

    constructor() {
        this.opcionTesinaListado = $('.tabs-transform').all(by.tagName('button')).get(1);
        this.botonCrear = $('ad-button-group button');
        this.opcionesFormacionAcademica = $('.big-buttons').all(by.tagName('a'));
        this.botonCerrarModal = $('.modal-header').$('[aria-label="Close"]');
    };
}
