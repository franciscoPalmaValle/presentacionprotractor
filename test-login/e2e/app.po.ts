import { browser, by, element, ElementFinder, protractor } from 'protractor';

const EC = protractor.ExpectedConditions;

export class AppPage {

  inputUsuario: ElementFinder;
  inputPassword: ElementFinder;
  btnEntrar: ElementFinder;

  constructor() {
    this.inputUsuario = element(by.id('usuario'));
    this.inputPassword = element(by.id('clave'));
    this.btnEntrar = element(by.css('.form-actions button'));
  }
  navigateToLogin(url: string) {
    return browser.get(url);
  }

  loginApp(user: string, pass: string) {
    this.inputUsuario.sendKeys(user);
    this.inputPassword.sendKeys(pass);
    this.btnEntrar.click();
    browser.wait(EC.visibilityOf(element(by.cssContainingText('h1', 'Convocatorias abiertas'))), 25000);
    return browser.getCurrentUrl();
  }
}
