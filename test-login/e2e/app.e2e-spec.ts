import { AppPage } from './app.po';
import { browser, ExpectedConditions, element, by } from 'protractor';

const page: AppPage = new AppPage();
const EC = ExpectedConditions;

describe('Test1 App. Prueba base app', () => {
  // TODO: Mostrar como falla la aplicación si no tenemos en cuenta la lentitud el entorno
  it('debemos acceder a la página del login', () => {
    const url = 'http://h15973.redocu.lan/develop/'
    page.navigateToLogin(url);
    browser.wait(EC.visibilityOf(element(by.id('usuario-info'))), 25000);
  });
  it('debemos loguearnos y acceder a la pantalla de Inicio', () => {
    expect(page.loginApp('rxalabarder', 'a')).toContain('/inicio');
  });
});